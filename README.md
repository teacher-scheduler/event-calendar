# Event Calendar in Django

Forked from [sajib1066/event-calendar](https://github.com/sajib1066/event-calendar)

## Render Deployment

### Prerequisites
1. Render account
2. Blank internal Postgres database from Render

### Settings

1. Build Command:
```bash
pip install --no-cache-dir poetry && python -m venv .venv && poetry install --no-root && DJANGO_ALLOWED_HOSTS=* DJANGO_SECRET_KEY=BUILDONLY poetry run python manage.py collectstatic --no-input && poetry run python manage.py migrate
```

2. Start Command:
```bash
poetry run gunicorn eventcalendar.wsgi:application --bind=0.0.0.0:8000 --workers 1 --log-level info
```

3. Health Check Path: `/ht/`

4. Environment Variables:

```
DATABASE_URL=                   # add internal database URL from render
DJANGO_ALLOWED_HOSTS=           # add hostname
DJANGO_DEBUG=False
DJANGO_MEDIA_PATH=/media/
DJANGO_MEDIA_URL=/media/
DJANGO_READ_DOT_ENV_FILE=False
DJANGO_SECRET_KEY=              # add secret key
DJANGO_SETTINGS_MODULE=eventcalendar.settings
```

## Creating superuser

1. Access database locally using External Database URL. Make sure to do this step after migration.
2. Run: `poetry run python manage.py createsuperuser`
